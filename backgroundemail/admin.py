# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin, messages
from django.utils.translation import ugettext_lazy as _

from .models import BackgroundEmail


def resend_emails(modeladmin, request, queryset):
    for email in queryset:
        email.send()
    messages.add_message(request, messages.SUCCESS, _('Great success!'))


resend_emails.short_description = _('Resend selected emails')


@admin.register(BackgroundEmail)
class BackgroundEmailAdmin(admin.ModelAdmin):
    actions = [resend_emails, ]
    list_display = ['id', 'template_name', 'recipient', 'created_at',
                    'is_sent', 'sent_at', 'is_failed']
    list_filter = ['is_sent', 'created_at', 'sent_at']
    search_fields = ['recipient', ]
    readonly_fields = ['created_at', ]

