# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def create_email(template_name, recipient_address, template_context=None,
                 send=True):
    # type: (str, str, dict, bool) -> object
    """
    Create BackgroundEmail to Django (and send it by default)

    :param template_name: Name of the email template
    :param recipient_address: Email address of the recipient
    :param template_context: Context for the email template
    :param send: Send the message right after it's created (default=True)
    :return: created BackgroundEmail
    """
    if template_context is None:
        template_context = {}

    from .models import BackgroundEmail

    email = BackgroundEmail.objects.create(
        template_name=template_name, recipient=recipient_address,
        template_context=template_context
    )
    if send:
        email.send()
    return email
