# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import threading
import logging

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone

from templated_email import send_templated_mail


logger = logging.getLogger(__name__)


class EmailThread(threading.Thread):
    def __init__(self, background_email):
        assert isinstance(background_email, BackgroundEmail)
        self.email = background_email
        threading.Thread.__init__(self)

    def run(self):
        try:
            send_templated_mail(
                template_name=self.email.template_name,
                from_email=settings.DEFAULT_FROM_EMAIL,
                recipient_list=[self.email.recipient, ],
                context=self.email.template_context
            )
            self.email.sent_at = timezone.now()
            self.email.is_sent = True
            logger.debug('BackgroundEmail {} sent!'.format(self.email.id))
        except Exception as e:
            logger.error(e.message)
            self.email.exception_message = e.message

        self.email.save()


class BackgroundEmail(models.Model):
    """ Very simple Background email """
    template_name = models.CharField(max_length=50)
    template_context = JSONField()
    recipient = models.EmailField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_sent = models.BooleanField(default=False)
    sent_at = models.DateTimeField(null=True, blank=True)
    exception_message = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return '"{}" email to {}'.format(self.template_name, self.recipient)

    @property
    def is_failed(self):
        if self.is_sent:  # probably succeeded if already sent...
            return False
        return self.exception_message != ''

    def send(self):
        logger.debug('Sending BackgroundEmail {}...'.format(self.id))
        EmailThread(self).start()
