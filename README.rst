===============
BackgroundEmail
===============

Very simple background email for Django. Emails are first saved to db as Django models, then they are sent by different thread.


Installation
============

Add ``backgroundemail`` to your ``INSTALLED_APPS``

.. code-block:: python

    INSTALLED_APPS = [
        # your other apps..
        'backgroundemail',
    ]

Run ``migrate``

.. code-block:: bash

    $ ./manage.py migrate


Requirements
============
- ``django-templated-email``
- Postgres (using ``django.contrib.postgres.fields.JSONField``)


How to use
==========

.. code-block:: python

    from backgroundemail import create_email


    email = create_email(
        template_name='welcome',  # Your templated email template name
        recipient_address='otto.lampila@gmail.com',
        template_context={'something': True},  # Context for the email template
    )


That's it, super easy! By default, emails are sent right after they are created.
If you want to send these emails later manually, then pass ``send=False`` to ``create_email()``

.. note:: This package does **NOT** include any email templates
