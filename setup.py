# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='backgroundemail',
    version='0.1',
    description='Very simple background email ',
    url='https://bitbucket.org/ottolampila/backgroundemail',
    author='Otto Lampila',
    author_email='otto.lampila@gmail.com',
    license='UNLICENSED',
    packages=['backgroundemail'],
    zip_safe=False,
    install_requires=['django-templated-email', ]
)
